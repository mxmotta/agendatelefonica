<?php

class Pessoa {

    private $id;
    private $nome;
    private $sexo;
    private $telefone;
    private $email;

    public function getId(){
        return $this->id;
    }

    public function getNome(){
        return $this->nome;
    }

    public function setNome($valor){
        $this->nome = $valor;
    }
    
    public function getSexo(){
        return $this->sexo;
    }

    public function setSexo($valor){
        $this->sexo = $valor;
    }
    
    public function getTelefone(){
        return $this->telefone;
    }

    public function setTelefone($valor){
        $this->telefone = $valor;
    }
    
    public function getEmail(){
        return $this->email;
    }

    public function setEmail($valor){
        $this->email = $valor;
    }

    public function listarPessoas(){

        $conexao = new Conexao();
        $link = $conexao->entrar();

        $sql = "SELECT * FROM pessoas";
        $query = $link->query($sql);
        
        while($return = mysqli_fetch_array($query)){
            $this->id[] = $return['id'];
            $this->nome[] = $return['nome'];
            $this->sexo[] = $return['sexo'];
            $this->telefone[] = $return['telefone'];
            $this->email[] = $return['email'];
        }

    }


} 