<?php 
require('./class/conexao.class.php');
require('./class/pessoa.class.php');

$pessoa = new Pessoa();

$pessoa->listarPessoas();
?>


<html>
<head>
    <title>Agenda Telefônica</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>


    <div class="container">

    <div class="text-center">
        <h3>Agenda Telefônica</h3>
    </div>

    <table class="table table-bordered table-striped">
        <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>Sexo</th>
            <th>Telefone</th>
            <th>Email</th>
        </tr>
        <?php for($i = 0; $i < count($pessoa->getId()); $i++): ?>

            <tr>
                <td><?= $pessoa->getId()[$i] ?></td>
                <td><?= $pessoa->getNome()[$i] ?></td>
                <td><?= $pessoa->getSexo()[$i] ?></td>
                <td><?= $pessoa->getTelefone()[$i] ?></td>
                <td><?= $pessoa->getEmail()[$i] ?></td>
            </tr>

        <?php endfor; ?>
    </table>

    </div>

</body>

</html>


